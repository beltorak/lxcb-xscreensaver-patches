###########################
# Initial Setup
###########################

build the packages and install them (need to install prereqs as well)

create /usr/share/pam-configs/z-custom-time-controls:
----
Name: Account Login Time Controls
Default: yes
Priority: 0
Account-Type: Additional
Account:
	required	pam_time.so	debug
Account-Initial:
	required	pam_time.so	debug
----

replace /usr/lib/kde4/libexec/kscreenlocker_greet with:
----
#!/bin/sh
exec /usr/bin/xscreensaver-command -lock
----

Create autostart file /etc/xdg/autostart/xscreensaver-command.desktop:
----
[Desktop Entry]
Comment[en_US]=
Comment=
Exec=/usr/bin/xscreensaver
GenericName[en_US]=
GenericName=
Icon=system-run
MimeType=
Name[en_US]=X Screen Saver
Name=X Screen Saver
Path=
StartupNotify=true
Terminal=false
TerminalOptions=
Type=Application
X-DBUS-ServiceName=
X-DBUS-StartupType=
X-KDE-SubstituteUID=false
X-KDE-Username=
----

###########################
# Enabling Time Restrictions
###########################
run pam-auth-update; ensure time controls is enabled

###########################
# Modifying Authorized Times
###########################
edit /etc/security/time.conf to limit login services: example:
	kdm | login | lightdm | gdm | xscreensaver ; * ; $user ; Al0800-2200
(services are listed in /etc/pam.d
